unit Form1U;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, ComCtrls,Types;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    ImageList1: TImageList;
    Label1: TLabel;
    PageControl1: TPageControl;
    Panel1: TPanel;
    Panel2: TPanel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation
uses
  wPlugin, Form2U, Form3U, Form4U;
{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin

  __wPluginSettings([TForm2,TForm3,TForm4],3);// указываем какие формы подгружать. 3 - загрузить первые 3 плагина
  //__wPluginSettings([TForm2,TForm3,TForm4],0);// указываем какие формы подгружать. 0 - не загружать плагины сразу

  __wPluginInit(Form1,PageControl1,self); // инициализация модуля wPlugin
end;

procedure TForm1.Button1Click(Sender: TObject);
begin

     Plugin.Add(TwPlugin.Create(0));//TForm2
end;

procedure TForm1.Button2Click(Sender: TObject);
begin

     Plugin.Add(TwPlugin.Create(1));//TForm3
end;

procedure TForm1.Button3Click(Sender: TObject);
begin

     Plugin.Add(TwPlugin.Create(2)); //TForm4
end;

end.

